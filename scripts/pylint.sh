checkFolder () {
  pylint ./$1/ --disable=missing-docstring,unused-argument --max-line-length=170
  if [ $? -ne 0 ]; then
    exit 1;
  fi
}

python3 -m pip install pylint 
python3 -m pip install -r ./src/requirements.txt
cp ./src/config.default.py ./src/config.py
checkFolder ./src
