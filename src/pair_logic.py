import static
import data
from message_utils import send_by_action_message
from message_utils import send_message
from message_utils import illegal_state_message_by_action


def handle_lawyer_pair(update, end_id, lawyer_roles, updater, data_db):
    query = update.callback_query
    # lock db???
    request_id = end_id
    request = data_db.get_request(request_id)
    current_lawyer = data_db.get_lawyer(update.callback_query.message.chat.id)
    if current_lawyer is None:
        illegal_state_message_by_action(query, update)
        return

    prisoner_user = request.prisoner.user
    client_info = static.USER_INFO.format(
        prisoner_user.name,
        '+' + prisoner_user.phone,
        '@' + prisoner_user.username if prisoner_user.username is not None else ''
    )

    current_lawyer_id = current_lawyer.user.id
    current_lawyer_info = static.USER_INFO.format(
        current_lawyer.user.name,
        '+' + current_lawyer.user.phone,
        '@' + current_lawyer.user.username if current_lawyer.user.username is not None else ''
    )
    request_lawyers = [
        request.prepare_docs_lawyer_id,
        request.go_in_ovd_lawyer_id,
        request.court_lawyer_id
    ]
    print("current_lawyer_id: %s" % current_lawyer_id)
    print("request_lawyers: %s" % request_lawyers)
    print("lawyer_roles: %s" % lawyer_roles)
    if request.status != data.RequestStatus.ACTIVE:
        if current_lawyer_id in request_lawyers:
            # this lawyer has already sent event for this client
            # send to lawyer
            send_by_action_message(
                query=query,
                text=static.LAWYER_PAIR_MESSAGE.format(client_info)
            )
            return
        send_by_action_message(
            query=query,
            text=static.CLIENT_HAS_BEEN_FOUND_LAWYERS_ALREADY_MESSAGE
        )
        return

    lawyer_actions_msg = []
    if request.prepare_docs_lawyer_id is None and data.LawyerRoles.GO_IN_OVD in lawyer_roles:
        lawyer_actions_msg.append(static.LAWYER_PREPARE_DOCS_MESSAGE)
        request.prepare_docs_lawyer_id = current_lawyer.user.id
    if request.go_in_ovd_lawyer_id is None and data.LawyerRoles.PREPARE_DOCS in lawyer_roles:
        lawyer_actions_msg.append(static.LAWYER_GO_IN_OVD_MESSAGE)
        request.go_in_ovd_lawyer_id = current_lawyer.user.id
    if request.court_lawyer_id is None and data.LawyerRoles.COURT in lawyer_roles:
        lawyer_actions_msg.append(static.LAWYER_COURT_MESSAGE)
        request.court_lawyer_id = current_lawyer.user.id

    if len(lawyer_actions_msg) == 0:
        # current lawyer has not been paired by role
        send_by_action_message(
            query=query,
            text=static.CLIENT_HAS_BEEN_FOUND_LAWYERS_ALREADY_MESSAGE
        )
        return

    if request.prepare_docs_lawyer_id is not None \
            and request.go_in_ovd_lawyer_id is not None \
            and request.court_lawyer_id is not None:
        request.status = data.RequestStatus.CLOSED

    request.save()

    # send to prisoner
    print(prisoner_user.external_id)
    print(current_lawyer_info)
    send_message(
        chat_id=prisoner_user.external_id,
        text=static.JOIN_LAWYER_MESSAGE.format(
            current_lawyer_info
        ),
        updater=updater
    )

    # send to lawyer
    send_by_action_message(
        query=query,
        text=static.LAWYER_PAIR_MESSAGE.format(
            client_info
        )
    )
