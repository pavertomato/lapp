import static
import config


def illegal_state_message_by_action(query, update):
    print("WARN: empty end_id for: %s" % update)
    send_by_action_message(query, static.ILLIGAL_STATE_MESSAGE)


def send_message(chat_id, text, updater, reply_markup=None):
    if config.TEST_MODE:
        print('send_message: chat_id: {0}, text: {1}, reply_markup: {2}', chat_id, text, reply_markup)
        return
    updater.bot.send_message(chat_id=chat_id, text=text, reply_markup=reply_markup)


def send_by_action_message(query, text):
    if config.TEST_MODE:
        print('send_by_action_message: query: {0}, text: {1}', query, text)
        return
    query.edit_message_text(text=text)
