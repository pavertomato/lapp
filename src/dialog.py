import logging
import telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

import static
import data
import pair_logic

from message_utils import send_by_action_message
from message_utils import send_message
from message_utils import illegal_state_message_by_action

MAX_NOTIFY_LAWYERS = 10000
REACTION_DELIMITER = '#'

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def start(update, context, data_db):
    keyboard = [[InlineKeyboardButton(
        static.START_MESSAGE_BUTTON, callback_data='agree')]]
    text = update.message.text
    if ' ' in text:
        token = data_db.get_token(text[text.find(' '):].strip())
        if token:
            data_db.delete_token(token)
            context.user_data.update({'token': 'ok'})

    context.user_data.update({'action': 'fill_name'})
    # keyboard = [[InlineKeyboardButton(static.START_MESSAGE_BUTTON, callback_data='notify_of_lawyers')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(static.START_MESSAGE, reply_markup=reply_markup)


def role(update, context, updater, data_db):
    keyboard = [[InlineKeyboardButton(static.USER_PRISONER_BUTTON, callback_data='prisoner')],]
    text = static.USER_OPTION_MESSAGE_NO_TOKEN
    if context.user_data.get('token') == "ok":
        keyboard.append([InlineKeyboardButton(static.USER_LAWYER_BUTTON, callback_data='lawyer')])
        text = static.USER_OPTION_MESSAGE_TOKEN
    reply_markup = InlineKeyboardMarkup(keyboard)
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=text, reply_markup=reply_markup)


def get_contact_info_prisoner(update, context, updater=None, data_db=None):
    return get_contact_info(update, context, updater, data_db, True)


def get_contact_info_lawyer(update, context, updater=None, data_db=None):
    return get_contact_info(update, context, updater, data_db, False)


def format_id(id0):
    return int(id0)


def get_contact_info(update, context, updater, data_db, prisoner):
    message = update.callback_query.message
    chat_id = format_id(message.chat.id)
    user = data_db.get_user(chat_id)
    text = static.CONTACTS_MESSAGE_LAWYER
    if prisoner:
        text = static.CONTACTS_MESSAGE_PRISONER
    if user is None:
        if prisoner:
            data_db.add_prisoner('', '', chat_id, '')
        else:
            data_db.add_lawyer('', '', chat_id, '')
    else:
        if prisoner:
            lawyer = data_db.get_lawyer(chat_id)
            if lawyer is not None:
                data_db.move_lawyer_to_prison(lawyer)
        else:
            prisoner = data_db.get_prisoner(chat_id)
            if prisoner is not None:
                data_db.move_prisoner_to_freedom(prisoner)

    info_keyboard = telegram.KeyboardButton(
        text=static.CONTACTS_REQUEST, request_contact=True, callback_data='contact')
    reply_markup = telegram.ReplyKeyboardMarkup([[info_keyboard]])
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=text,
                             reply_markup=reply_markup)


def get_geo_info(update, context, updater=None, data_db=None):
    message = update.message
    chat_id = format_id(message.chat.id)
    lawyer = data_db.get_lawyer(chat_id)
    if lawyer is not None:
        user = lawyer.user
        data_db.update_lawyer(user.id, message.chat.first_name,
                              message.contact.phone_number, chat_id,
                              message.chat.username,
                              user.latitude, user.longitude,
                              lawyer.wantCanGoToPolice,
                              lawyer.wantCanWriteComplaint,
                              lawyer.wantCanGoToCourt,
                              lawyer.status)
    else:
        prisoner = data_db.get_prisoner(chat_id)
        if prisoner is None:
            print('user not found')
        else:
            user = prisoner.user
            try:
                data_db.update_prisoner(user.id, message.chat.first_name,
                                        message.contact.phone_number, chat_id,
                                        message.chat.username,
                                        user.latitude, user.longitude)
            except ValueError as ex:
                print(ex)

    context.user_data.update({'action': 'fill_name'})
    context.bot.send_message(chat_id=chat_id, text=static.ENTER_YOUR_NAME)


def fill_name(update, context, updater, data_db):
    message = update.message
    chat_id = format_id(message.chat.id)
    user = data_db.get_user(chat_id)
    data_db.update_user(user.id, message.text,
                        user.phone, chat_id, user.username,
                        user.latitude, user.longitude)
    text = static.GEO_MESSAGE_LAWYER
    lawyer = data_db.get_lawyer(chat_id)
    if lawyer is None:
        text = static.GEO_MESSAGE_PRISONER

    info_keyboard = telegram.KeyboardButton(text=static.GEO_REQUEST,
                                            request_location=True,
                                            callback_data='geo')
    reply_markup = telegram.ReplyKeyboardMarkup([[info_keyboard]])
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=text,
                             reply_markup=reply_markup)


def geo_handler(update, context, updater, data_db):
    message = update.message
    location = message.location
    if location is None:
        print("can't find location")
    else:
        chat_id = format_id(message.chat.id)
        lawyer = data_db.get_lawyer(chat_id)
        if lawyer is not None:
            user = lawyer.user
            data_db.update_lawyer(user.id, user.name,
                                  user.phone, chat_id, user.username,
                                  location.latitude,
                                  location.longitude,
                                  lawyer.wantCanGoToPolice,
                                  lawyer.wantCanWriteComplaint,
                                  lawyer.wantCanGoToCourt,
                                  lawyer.status)

            reply_markup = telegram.ReplyKeyboardRemove()
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Геоданные получены",
                                     reply_markup=reply_markup)
        else:
            prisoner = data_db.get_prisoner(chat_id)
            if prisoner is None:
                print('user not found')
            else:
                user = prisoner.user
                data_db.update_prisoner(user.id, user.name,
                                        user.phone, chat_id, user.username,
                                        location.latitude,
                                        location.longitude)
                chat_id = update.message.chat.id
                context.user_data.update({'action': 'fill_info'})
                context.bot.send_message(chat_id=chat_id, text=static.ENTER_YOUR_INFO)


def do_nothing(update, context, updater, data_db):
    reply_markup = telegram.ReplyKeyboardRemove()
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text="Хорошего дня :)",
                             reply_markup=reply_markup)


def lawyer_ready_reaction(update, context, updater, data_db):
    end_id = update.callback_query.data.split(REACTION_DELIMITER)[1]
    lawyer_roles = [
        data.LawyerRoles.GO_IN_OVD,
        data.LawyerRoles.PREPARE_DOCS,
        data.LawyerRoles.COURT
    ]
    pair_logic.handle_lawyer_pair(
        update, end_id, lawyer_roles, updater, data_db)


def lawyer_ready_to_go_in_ovd_reaction(update, context, updater, data_db):
    end_id = update.callback_query.data.split(REACTION_DELIMITER)[1]
    lawyer_roles = [
        data.LawyerRoles.GO_IN_OVD
    ]
    pair_logic.handle_lawyer_pair(
        update, end_id, lawyer_roles, updater, data_db)


def lawyer_ready_to_prepare_docs_reaction(update, context, updater, data_db):
    end_id = update.callback_query.data.split(REACTION_DELIMITER)[1]
    lawyer_roles = [
        data.LawyerRoles.PREPARE_DOCS,
    ]
    pair_logic.handle_lawyer_pair(
        update, end_id, lawyer_roles, updater, data_db)


def lawyer_ready_to_court(update, context, updater, data_db):
    end_id = update.callback_query.data.split(REACTION_DELIMITER)[1]
    lawyer_roles = [
        data.LawyerRoles.COURT
    ]
    pair_logic.handle_lawyer_pair(
        update, end_id, lawyer_roles, updater, data_db)


def lawyer_reject(update, context, updater, data_db):
    query = update.callback_query
    send_by_action_message(
        query=query,
        text=static.LAWYER_REJECT_ANSWER_MESSAGE
    )


def build_reaction_id(prefix, sub_id):
    return "%s%s%s" % (prefix, REACTION_DELIMITER, sub_id)


def notify_of_lawyers(update, context, updater, data_db):
    info = update.message.text
    if info is None:
        info = ''
    print(update.message)
    chat_id = update.message.chat.id
    prisoner = data_db.get_prisoner(format_id(chat_id))
    if prisoner is None:
        illegal_state_message_by_action(update, context)
        return
    lawyers = list(data_db.get_lawyers_by_distance(prisoner))
    # lawyers.append(data_db.get_lawyer(108227187))
    request = data_db.add_request(
        prisoner, lawyers, info, data.RequestStatus.ACTIVE)

    if lawyers is None or len(lawyers) == 0:
        send_message(
            chat_id=chat_id,
            text=static.NOT_FOUND_LAWYERS_MESSAGE,
            updater=updater
        )
        return
    keyboard = [
        [InlineKeyboardButton(
            static.LAWYER_RESPONSE_1,
            callback_data=build_reaction_id('lawyer_ready_reaction', request.id)
        )],
        [InlineKeyboardButton(
            static.LAWYER_RESPONSE_2,
            callback_data='lawyer_reject'
        )]
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    for lawyer in lawyers[0:MAX_NOTIFY_LAWYERS]:
        print(lawyer.user.phone)
        send_message(
            chat_id=lawyer.user.external_id,
            text=static.LAWYER_REQUEST_MESSAGE.format(request.info),
            updater=updater,
            reply_markup=reply_markup
        )


CALLBACK_TABLE = {
    'agree': role,
    'prisoner': get_contact_info_prisoner,
    'lawyer': get_contact_info_lawyer,
    'contact': get_geo_info,
    'geo': geo_handler,
    'lawyer_ready_reaction': lawyer_ready_reaction,
    'lawyer_reject': lawyer_reject,
    "notify_of_lawyers": notify_of_lawyers
}
