#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import argparse
from telegram.ext import Updater, CommandHandler, MessageHandler, CallbackQueryHandler, Filters

import data
import dialog

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

GENERATORS = ["artfulcucumber", "lelush", "lionisnotacake", "PospesilNasmesil"]

def main():
    """Run the bot."""
    global updater # pylint: disable=global-variable-undefined,invalid-name
    global data_db # pylint: disable=global-variable-undefined,invalid-name

    data_db = data.Data()
    data_db.start()
    parser = argparse.ArgumentParser(description="sample bot")
    parser.add_argument("token", type=str, help="api token")
    args = parser.parse_args()
    # Telegram Bot Authorization Token
    updater = Updater(args.token, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', lambda update, context: dialog.start(update, context, data_db)))
    updater.dispatcher.add_handler(CommandHandler('status', status))
    updater.dispatcher.add_handler(CommandHandler('token', token))
    updater.dispatcher.add_handler(CommandHandler('start_request', start_request))
    # updater.dispatcher.add_handler(CommandHandler('info', info))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, handle_text))
    updater.dispatcher.add_handler(MessageHandler(Filters.location, handle_geo))
    updater.dispatcher.add_handler(MessageHandler(Filters.contact, handle_contact))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    # updater.dispatcher.add_error_handler(error)
    # Start the Bot
    updater.start_polling()

    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


def start_request(update, context):
    chat_id = update.message.chat.id
    context.user_data.update({'action': 'fill_info'})
    context.bot.send_message(chat_id=chat_id, text="Пожалуйста, опишите текстом вашу ситуацию:")


def status(update, context):
    chat_id = update.message.chat.id
    lawyer = data_db.get_lawyer(chat_id)
    if lawyer is None:
        context.bot.send_message(chat_id=chat_id, text="401")
        return
    message = "теперь вы принимаете дела"
    lawyer_status = 1
    if lawyer.status == 1:
        lawyer_status = 0
        message = "теперь вы не принимаете дела"
    data_db.update_lawyer_status(lawyer=lawyer, status=lawyer_status)
    context.bot.send_message(chat_id=chat_id, text=message)

def token(update, context):
    if update.message.chat.username in GENERATORS:
        context.bot.send_message(chat_id=update.message.chat.id, text=str(data_db.add_token().token))

def button(update, context):
    start_id = update.callback_query.data.split(dialog.REACTION_DELIMITER)[0]
    dialog.CALLBACK_TABLE.get(start_id, dialog.do_nothing)(update, context, updater, data_db)


def handle_geo(update, context):
    dialog.CALLBACK_TABLE.get('geo', dialog.do_nothing)(update, context, updater, data_db)


def handle_contact(update, context):
    dialog.CALLBACK_TABLE.get('contact', dialog.do_nothing)(update, context, updater, data_db)


def handle_text(update, context):
    if 'action' in context.user_data:
        action = context.user_data['action']
        if action == 'fill_info': # pylint: disable=no-else-return
            dialog.notify_of_lawyers(update, context, updater, data_db)
            return
        elif action == "fill_name":
            dialog.fill_name(update, context, updater, data_db)
            return
        else:
            dialog.CALLBACK_TABLE.get('text', dialog.do_nothing)(update, context, updater, data_db)
            return
    else:
        dialog.CALLBACK_TABLE.get('text', dialog.do_nothing)(update, context, updater, data_db)
        return

if __name__ == '__main__':
    main()
