#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import datetime # pylint: disable=all
import decimal
from math import sin, cos, sqrt, atan2, radians
from peewee import * # pylint: disable=unused-wildcard-import
import config
import static
import uuid

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

R = 6373.0

user = 'rest'
password = 'test'
db_name = 'resttest'

dbhandle = PostgresqlDatabase(
    db_name, user=user,
    password=password,
    host=config.HOST
)

class RequestStatus:
    ACTIVE = 0
    CLOSED = 1
    DRAFT = 2

class LawyerRoles:
    GO_IN_OVD = 1
    PREPARE_DOCS = 2
    COURT = 3

class BaseModel(Model):
    class Meta:
        database = dbhandle

class User(BaseModel):
    id = PrimaryKeyField(null=False)
    name = CharField(max_length=100)
    phone = CharField(max_length=100)
    external_id = IntegerField(unique=True) # chat id
    username = CharField(max_length=100, null=True, default=None)
    latitude = DecimalField(max_digits=15, decimal_places=10, default=None, null=True) 
    longitude = DecimalField(max_digits=15, decimal_places=10, default=None, null=True) 

    created_at = DateTimeField(default=datetime.datetime.now())
    updated_at = DateTimeField(default=datetime.datetime.now())

    class Meta:
        db_table = "poc"
        order_by = ('created_at',)

class Prisoner(BaseModel):
    user = ForeignKeyField(User, primary_key=True)

    class Meta:
        db_table = "prisoner"
        order_by = ('created_at',)


class Token(BaseModel):
    id = PrimaryKeyField(null=False)
    token = CharField(max_length=100, null=False)
    created_at = DateTimeField(default=datetime.datetime.now())
    status = SmallIntegerField(default=1)

    class Meta:
        db_table = "token"


class Lawyer(BaseModel):
    user = ForeignKeyField(User, primary_key=True)
    wantCanGoToPolice = BooleanField(default=True)
    wantCanWriteComplaint = BooleanField(default=True)
    wantCanGoToCourt = BooleanField(default=True)
    status = SmallIntegerField()

    class Meta:
        db_table = "lawyer"
        order_by = ('created_at',)

class Distance(BaseModel):
    lawyer = ForeignKeyField(User)
    prisoner = ForeignKeyField(User)
    distance = DecimalField(max_digits=15, decimal_places=10, default=None, null=True) 

    class Meta:
        db_table = "distance"

class Request(BaseModel):
    id = PrimaryKeyField(null=False)
    prisoner = ForeignKeyField(Prisoner, backref='requests')
    lawyers = ManyToManyField(Lawyer, backref='requests')
    prepare_docs_lawyer_id = IntegerField(null = True)
    go_in_ovd_lawyer_id = IntegerField(null = True)
    court_lawyer_id = IntegerField(null = True)
    info = TextField()
    status = SmallIntegerField()

    created_at = DateTimeField(default=datetime.datetime.now())
    updated_at = DateTimeField(default=datetime.datetime.now())

    class Meta:
        db_table = "request"
        order_by = ('created_at',)

class Data:
    def get_or_create(self, external_id, get, func):
        with dbhandle.atomic():
            user = get(external_id)
            if user is not None :
                return user
            return func(external_id)

    def add_token(self):
        return Token.create(token=uuid.uuid4())
        
    def delete_token(self, token):
        return Token.delete().where(Token.token == token.token).execute()

    def add_user(self, name, phone, external_id, username=None,
            latitude=None, longitude=None):
        if username:
            username = username.strip()
        return User.create(
            name=name.strip(),
            phone=phone.lower().strip(),
            username=username,
            external_id=external_id,
            latitude = latitude,
            longitude = longitude,
            created_at = datetime.datetime.now()
        )

    def update_user(self, id, name, phone, external_id, username=None,
            latitude=None, longitude=None):
        user = User(id = id,
            name=name.strip(),
            phone=phone.lower().strip(),
            external_id=external_id,
            username = username,
            latitude = latitude,
            longitude = longitude,
        )
        user.save()
        return user

    def add_prisoner(self, name, phone, external_id, username=None,
            latitude=None, longitude=None):
        user = self.add_user(name, phone, external_id, username, latitude, longitude)

        return self.add_prisoner_user(user)

    def add_prisoner_user(self, user):
        row = Prisoner.create(
            user=user,
        )
        self.populate_distances_for_prisoner(user)
        return row

    def update_prisoner(self, id, name, phone, external_id, username=None,
            latitude=None, longitude=None):
        user = self.update_user(id, name, phone, external_id, username, latitude, longitude)

        row = Prisoner(
            user=user,
        )
        Distance.delete().where(Distance.prisoner == user).execute()
        self.populate_distances_for_prisoner(user)
        return row

    def add_lawyer(self, name, phone, external_id, username=None, latitude=None, longitude=None, wantCanGoToPolice=True, wantCanWriteComplaint=True, wantCanGoToCourt=True, status=1):
        user = self.add_user(name, phone, external_id, username, latitude, longitude)
        return self.add_lawyer_user(user, wantCanGoToPolice = wantCanGoToPolice, wantCanWriteComplaint = wantCanWriteComplaint, wantCanGoToCourt = wantCanGoToCourt, status = status)

    def add_lawyer_user(self, user, wantCanGoToPolice=True, wantCanWriteComplaint=True, wantCanGoToCourt=True, status=1):
        row = Lawyer.create(
            user=user,
            wantCanGoToPolice = wantCanGoToPolice,
            wantCanWriteComplaint = wantCanWriteComplaint,
            wantCanGoToCourt = wantCanGoToCourt,
            status = status
        )
        self.populate_distances_for_lawyer(user)
        return row

    def update_lawyer(self, id, name, phone, external_id, username=None,
            latitude=None, longitude=None,
            wantCanGoToPolice=True, wantCanWriteComplaint=True, wantCanGoToCourt=True,
            status=1):
        user = self.update_user(id, name, phone, external_id, username, latitude, longitude)
        row = Lawyer(
            user=user,
            wantCanGoToPolice = wantCanGoToPolice,
            wantCanWriteComplaint = wantCanWriteComplaint,
            wantCanGoToCourt = wantCanGoToCourt,
            status = status
        )
        Distance.delete().where(Distance.lawyer == user).execute()
        self.populate_distances_for_lawyer(user)
        row.save()
        return row

    def add_request(self, prisoner, lawyers, info, status):
        row = Request.create(
            prisoner = prisoner,
            info = info,
            status = status,
            created_at = datetime.datetime.now()
        )
        row.lawyers.add(lawyers)
        return row

    def update_request(
        self, id, prisoner, lawyers, info, status, 
        prepare_docs_lawyer_id = None, 
        go_in_ovd_lawyer_id = None, 
        court_lawyer_id = None
    ):
        row = Request(
            id = id,
            prisoner = prisoner,
            info = info,
            status = status,
            prepare_docs_lawyer_id = prepare_docs_lawyer_id,
            go_in_ovd_lawyer_id = go_in_ovd_lawyer_id,
            court_lawyer_id = court_lawyer_id
        )
        row.lawyers.clear()
        row.lawyers.add(lawyers)
        row.save()
        return row

    def get_token(self, token_str):
        token = Token.get_or_none(token=token_str)
        return token

    def get_user(self, external_id):
        user = User.get_or_none(external_id=external_id)
        return user

    def get_prisoner(self, external_id):
        user = User.get_or_none(external_id=external_id)
        if user == None :
            return None
        return Prisoner.get_or_none(user=user) 

    def get_lawyer(self, external_id):
        user = User.get_or_none(external_id=external_id)
        if user == None :
            return None
        return Lawyer.get_or_none(user=user) 

    def get_request(self, request_id):
        request = Request.get_or_none(id = request_id)
        return request

    @dbhandle.atomic()
    def move_lawyer_to_prison(self, lawyer):
        user = lawyer.user
        id = user.id
        ThroughModel = Request.lawyers.get_through_model()
        ThroughModel.delete().where(ThroughModel.lawyer == lawyer).execute()
        Distance.delete().where(Distance.lawyer == user).execute()
        Lawyer.delete().where(Lawyer.user==user).execute()
        User.delete().where(User.id==id).execute()
        self.add_prisoner(user.name, user.phone, user.external_id, user.username, user.latitude, user.longitude)

    @dbhandle.atomic()
    def move_prisoner_to_freedom(self, prisoner):
        user = prisoner.user
        id = user.id
        ids = Request.select(Request.id).where(Request.prisoner == prisoner).tuples()
        ThroughModel = Request.lawyers.get_through_model()
        ThroughModel.delete().where(ThroughModel.request_id in ids).execute()
        Request.delete().where(Request.prisoner == prisoner).execute()
        Distance.delete().where(Distance.prisoner == user).execute()
        Prisoner.delete().where(Prisoner.user==user).execute()
        User.delete().where(User.id==id).execute()
        lawyer = self.add_lawyer(user.name, user.phone, user.external_id, user.username, user.latitude, user.longitude)

    def start(self):
        try:
            dbhandle.connect()
            dbhandle.create_tables((User, Prisoner, Lawyer, Distance, Request, Token, Request.lawyers.get_through_model()))
            Lawyer.create_table()
            Request.create_table()
            User.create_table()
            Token.create_table()
        except InternalError as px:
            print(str(px))
        # prisoner = self.get_or_create(56851573, lambda external_id: self.get_prisoner(external_id),
        #         lambda x: self.add_prisoner("Looking Forhalp", "putin.vor@sibmail.com", "+73069136300", x , latitude=56.6320880000, longitude=24.9439402000, username="PospesilNasmesil"))
        # lawyer = self.get_or_create(108227187, lambda external_id: self.get_lawyer(external_id),
        #         lambda x: self.add_lawyer("Justice Good", "kittie.lover@sibmail.com", "+9962368215392", x, latitude=56.4320880000, longitude=24.7439402000, username="PospesilNasmesil"))
        # self.update_lawyer(lawyer.user.id, lawyer.user.name, lawyer.user.email, lawyer.user.phone, lawyer.user.external_id, latitude=lawyer.user.latitude, longitude=lawyer.user.longitude, status=1)
        # self.add_request(prisoner, [lawyer], "rrr", 0)
        # request = self.add_request(prisoner, [lawyer], "", 0)
        # self.update_request(request.id, prisoner, [lawyer], "done", 1)

    def populate_distances_for_lawyer(self, lawyer):
        for prisoner in User.select(User).join(Prisoner, JOIN.INNER):
            Distance.create(
                lawyer = lawyer,
                prisoner = prisoner,
                distance = decimal.Decimal(calculate_distance(lawyer.latitude, lawyer.longitude, prisoner.latitude, prisoner.longitude))
                )

    def populate_distances_for_prisoner(self, prisoner):
        for lawyer in User.select(User).join(Lawyer, JOIN.INNER):
            distance = decimal.Decimal(calculate_distance(lawyer.latitude, lawyer.longitude, prisoner.latitude, prisoner.longitude))
            Distance.create(
                lawyer = lawyer,
                prisoner = prisoner,
                distance=distance)    

    def get_lawyers_by_distance(self, prisoner, limit = 10, offset = 0):
        #query = Lawyer.select(Lawyer.user, Lawyer.status, Distance.lawyer, Distance.prisoner).join(Distance, on=(Distance.lawyer == Lawyer.user)).where(Distance.prisoner == prisoner.user and Lawyer.status==1).tuples()
        return Lawyer.select(Lawyer, Distance).join(Distance, on=(Distance.lawyer == Lawyer.user)).where(Lawyer.status==1 and Distance.distance<static.MAX_DISTANCE).order_by(Distance.distance).switch(Distance).where(Distance.prisoner == prisoner.user).limit(limit).offset(offset).execute()

    def update_lawyer_status(self, lawyer, status):
        self.update_lawyer(lawyer.user.id, lawyer.user.name, lawyer.user.phone,
                      lawyer.user.external_id, latitude=lawyer.user.latitude, longitude=lawyer.user.longitude, status=status)

def calculate_distance(lat1,lon1,lat2,lon2):
    distance = 0
    try:
        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        angle = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        coefficient = 2 * atan2(sqrt(angle), sqrt(1 - angle))
        distance = R * coefficient
    except TypeError as type_error:
            print(str(type_error))
    return distance
